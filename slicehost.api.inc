<?php

/**
 * @file
 */

/**
 * Get the API information from slicehost
 */
function slicehost_api_info() {
  $xml = slicehost_api_request('api');
  
  if ($xml) {
    $info = array();
    
    foreach ($xml as $key => $value) {
      $info[$key] = (string)$value;
    }
    
    return $info;
  }
  return FALSE;
}

function slicehost_api_list($name = NULL) {
  $slices = array();
  $params = array();
  if ($name) {
    $params['name'] = $name;
  }
  
  $xml = slicehost_api_request('slices', NULL, $params);

  foreach ($xml as $element) {
    $slices[(string)$element->name] = $element;
  }

  $slices = array_map('_slicehost_api_map_slices', $slices);
  
  return $slices;
}

function _slicehost_api_map_slices($a) {
  $b = array();
  
  foreach ($a as $key => $value) {
    if ($key == 'addresses') {
      $b['addresses'] = array();
      foreach ($value as $address) {
        $b['addresses'][] = (string)$address;
      }
    }
    else {
      $b[$key] = (string)$value;
    }
  }
  return $b;
}

/**
 * Reboot Slice
 */
function slicehost_api_reboot($hard = FALSE) {
  
}

/**
 * Send request to slicehost api
 */
function slicehost_api_request($command, $path = NULL, $params = array(), $method = 'GET') {
  $apikey = variable_get('slicehost_api_key', FALSE);
  
  if (!$apikey) {
    return FALSE;
  }
  
  $uri = 'https://' . $apikey . '@api.slicehost.com';
  
  if ($path) {
    $uri .= '/' . $path;
  }
  
  $uri .= '/' . $command .'.xml';
  
  $result = drupal_http_request($uri, array(), $method);
  
  if ($result->code == 200) {
    $xml = simplexml_load_string($result->data);
    
    return $xml;
  }
  else {
    drupal_set_message(t('Problem communicating with Slicehost. (@code - @description)', array('@code' => $result->code, '@description' => $result->error)));
  }
  
  return FALSE;
}

