<?php

/**
 * Slicehost api settings
 */
function slicehost_api_settings() {
  $form = array();
  
  if ($apikey = variable_get('slicehost_api_key', '')) {
    slicehost_api_include();
  
    $info = slicehost_api_info();
    if ($info) {
      $form['info'] = array(
        '#type' => 'fieldset',
        '#title' => t('Slicehost API information'),
      );
  
      foreach ($info as $key => $value) {
        $form['info'][$key] = array(
          '#type' => 'item',
          '#title' => drupal_ucfirst($key),
          '#value' => $value,
        );
      }
    }
    else {
      drupal_set_message(t('There was a problem getting the slicehost api information.'));
    }
  }
  
  $form['slicehost_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Password'),
    '#default_value' => $apikey,
    '#description' => t('The API access key needs to be generated in the Slicehost management account at https://manage.slicehost.com/api/'),
  );
  
  return system_settings_form($form);
}